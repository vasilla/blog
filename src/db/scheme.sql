--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

-- Started on 2018-07-17 15:39:26

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2867 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 35697)
-- Name: article_tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.article_tag (
    article_id bigint NOT NULL,
    tag_id bigint NOT NULL
);


ALTER TABLE public.article_tag OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 35700)
-- Name: articles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.articles (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    published boolean NOT NULL,
    published_date timestamp(4) without time zone,
    last_updated date NOT NULL,
    html text,
    image_name text,
    blocked_id text
);


ALTER TABLE public.articles OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 35706)
-- Name: articles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.articles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_id_seq OWNER TO postgres;

--
-- TOC entry 2868 (class 0 OID 0)
-- Dependencies: 198
-- Name: articles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.articles_id_seq OWNED BY public.articles.id;


--
-- TOC entry 199 (class 1259 OID 35708)
-- Name: blocked; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blocked (
    reason text NOT NULL,
    user_id bigint NOT NULL,
    id text NOT NULL
);


ALTER TABLE public.blocked OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 35714)
-- Name: coautors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.coautors (
    id bigint NOT NULL,
    article_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.coautors OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 35717)
-- Name: coautors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.coautors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.coautors_id_seq OWNER TO postgres;

--
-- TOC entry 2869 (class 0 OID 0)
-- Dependencies: 201
-- Name: coautors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.coautors_id_seq OWNED BY public.coautors.id;


--
-- TOC entry 202 (class 1259 OID 35719)
-- Name: personnel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.personnel (
    role_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.personnel OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 35722)
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    name text,
    can_edit_roles boolean,
    can_block_users boolean,
    can_edit_personnel boolean,
    can_block_article boolean,
    can_edit_tag boolean,
    id bigint NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 35728)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- TOC entry 2870 (class 0 OID 0)
-- Dependencies: 204
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- TOC entry 205 (class 1259 OID 35730)
-- Name: tags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tags (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.tags OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 35736)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    username text NOT NULL,
    login text NOT NULL,
    password text NOT NULL,
    blocked_id text
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 35742)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2871 (class 0 OID 0)
-- Dependencies: 207
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2709 (class 2604 OID 35744)
-- Name: articles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.articles ALTER COLUMN id SET DEFAULT nextval('public.articles_id_seq'::regclass);


--
-- TOC entry 2710 (class 2604 OID 35745)
-- Name: coautors id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coautors ALTER COLUMN id SET DEFAULT nextval('public.coautors_id_seq'::regclass);


--
-- TOC entry 2711 (class 2604 OID 35746)
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- TOC entry 2712 (class 2604 OID 35747)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2717 (class 2606 OID 35749)
-- Name: articles articles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT articles_pkey PRIMARY KEY (id);


--
-- TOC entry 2719 (class 2606 OID 35751)
-- Name: coautors coautors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coautors
    ADD CONSTRAINT coautors_pkey PRIMARY KEY (id);


--
-- TOC entry 2725 (class 2606 OID 35753)
-- Name: roles roles_id_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_id_pk PRIMARY KEY (id);


--
-- TOC entry 2728 (class 2606 OID 35755)
-- Name: tags tags_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- TOC entry 2730 (class 2606 OID 35757)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2715 (class 1259 OID 35758)
-- Name: articles_fkey_users; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX articles_fkey_users ON public.articles USING btree (user_id);


--
-- TOC entry 2713 (class 1259 OID 35759)
-- Name: fki_article_key; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_article_key ON public.article_tag USING btree (article_id);


--
-- TOC entry 2720 (class 1259 OID 35760)
-- Name: fki_coauthors_article; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_coauthors_article ON public.coautors USING btree (article_id);


--
-- TOC entry 2721 (class 1259 OID 35761)
-- Name: fki_coautots_user; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_coautots_user ON public.coautors USING btree (user_id);


--
-- TOC entry 2722 (class 1259 OID 35762)
-- Name: fki_personnel_role; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_personnel_role ON public.personnel USING btree (role_id);


--
-- TOC entry 2723 (class 1259 OID 35763)
-- Name: fki_personnel_user; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_personnel_user ON public.personnel USING btree (user_id);


--
-- TOC entry 2726 (class 1259 OID 35764)
-- Name: fki_tag_author; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_tag_author ON public.tags USING btree (user_id);


--
-- TOC entry 2714 (class 1259 OID 35765)
-- Name: fki_tag_key; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_tag_key ON public.article_tag USING btree (tag_id);


--
-- TOC entry 2731 (class 2606 OID 35766)
-- Name: article_tag article_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.article_tag
    ADD CONSTRAINT article_key FOREIGN KEY (article_id) REFERENCES public.articles(id) ON DELETE CASCADE;


--
-- TOC entry 2733 (class 2606 OID 35771)
-- Name: articles articles_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT articles_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 2734 (class 2606 OID 35776)
-- Name: coautors coauthors_article; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coautors
    ADD CONSTRAINT coauthors_article FOREIGN KEY (article_id) REFERENCES public.articles(id);


--
-- TOC entry 2735 (class 2606 OID 35781)
-- Name: coautors coautots_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coautors
    ADD CONSTRAINT coautots_user FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 2736 (class 2606 OID 35786)
-- Name: personnel personnel_role; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personnel
    ADD CONSTRAINT personnel_role FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- TOC entry 2737 (class 2606 OID 35791)
-- Name: personnel personnel_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personnel
    ADD CONSTRAINT personnel_user FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 2738 (class 2606 OID 35796)
-- Name: tags tag_author; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tag_author FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 2732 (class 2606 OID 35801)
-- Name: article_tag tag_key; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.article_tag
    ADD CONSTRAINT tag_key FOREIGN KEY (tag_id) REFERENCES public.tags(id) ON DELETE CASCADE;


-- Completed on 2018-07-17 15:39:26

--
-- PostgreSQL database dump complete
--

