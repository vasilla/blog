--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

-- Started on 2018-07-17 15:40:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2870 (class 0 OID 35736)
-- Dependencies: 206
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users (id, username, login, password, blocked_id) VALUES (1, 'admin', 'admin', '$2a$12$K6hznEI0EJTLFcvOK8ZdcuXLyuJityjCh3yRCFXc9QtazxTMBCNHG', NULL);
INSERT INTO public.users (id, username, login, password, blocked_id) VALUES (3, 'user #2', 'user2', '$2a$12$bqJmtxZ6AVaolnDFmU3BDeHa6/.YIKr59qoHJWULm2EuW3aHxrObC', NULL);
INSERT INTO public.users (id, username, login, password, blocked_id) VALUES (2, 'user #1', 'user', '$2a$12$6CZRdVRP1XpUnuxAoQJEJeKsg..m8FmNrRkKJbodUON.LpaQddCUm', NULL);


--
-- TOC entry 2861 (class 0 OID 35700)
-- Dependencies: 197
-- Data for Name: articles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (2, 2, 'post #2
', 'post #1 by user #1', false, NULL, '2018-07-02', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (3, 2, 'post #3', 'post #2 by user #1', true, '2018-07-02 00:00:00', '2018-07-02', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (4, 3, 'post #4', 'post #1 by user #2', false, NULL, '2018-07-02', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (10, 2, 'someTitle#5', 'created by user #1', true, '2018-07-04 00:00:00', '2018-07-04', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (7, 2, 'someTitle#2', 'created by user #1', true, '2018-07-04 00:00:00', '2018-07-04', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (6, 2, 'someTitle#1', 'created by user #1', true, '2018-07-04 00:00:00', '2018-07-04', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (13, 2, 'someTitle#8', 'created by user #1', true, '2018-07-04 00:00:00', '2018-07-04', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (12, 2, 'someTitle#7', 'created by user #1', true, '2018-07-04 00:00:00', '2018-07-04', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (14, 2, 'someTitle#9', 'created by user #1', true, '2018-07-04 00:00:00', '2018-07-04', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (9, 2, 'someTitle#4', 'created by user #1', true, '2018-07-04 00:00:00', '2018-07-04', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (8, 2, 'someTitle#3', 'created by user #1', true, '2018-07-04 00:00:00', '2018-07-04', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (11, 2, 'someTitle#6', 'created by user #1', true, '2018-07-04 00:00:00', '2018-07-04', NULL, '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (19, 1, 'asasd', 'asdasda', false, NULL, '2018-07-13', '<p><em>Your</em> content <strong>here&nbsp;</strong><img src="test" alt="image" width=""></p>', '404.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (26, 1, 'article + image4', 'asasda', false, NULL, '2018-07-14', NULL, 'http://www.apicius.es/wp-content/uploads/2012/07/IMG-20120714-009211.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (27, 1, 'article cat', 'cat updated', true, '2018-07-16 07:28:15.1446', '2018-07-16', '<p>http://<strong>localhost</strong>:8080/<em>article</em>/edit/<sub>html</sub>?article=<em>27</em></p>', 'https://images.pexels.com/photos/20787/pexels-photo.jpg', NULL);
INSERT INTO public.articles (id, user_id, title, description, published, published_date, last_updated, html, image_name, blocked_id) VALUES (25, 1, 'title', 'qwqweq', true, '2018-07-16 07:28:51.253', '2018-07-16', NULL, 'b73b575e-1e4d-4eae-913f-cabab5580e86.png', NULL);


--
-- TOC entry 2869 (class 0 OID 35730)
-- Dependencies: 205
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.tags (id, user_id, name) VALUES (1, 1, 'tag#1');
INSERT INTO public.tags (id, user_id, name) VALUES (2, 1, 'tag#2');
INSERT INTO public.tags (id, user_id, name) VALUES (3, 1, 'tag#3');
INSERT INTO public.tags (id, user_id, name) VALUES (4, 1, 'tag#4');
INSERT INTO public.tags (id, user_id, name) VALUES (5, 1, 'tag#5');
INSERT INTO public.tags (id, user_id, name) VALUES (6, 1, 'taggggggs');


--
-- TOC entry 2860 (class 0 OID 35697)
-- Dependencies: 196
-- Data for Name: article_tag; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.article_tag (article_id, tag_id) VALUES (27, 2);
INSERT INTO public.article_tag (article_id, tag_id) VALUES (27, 4);
INSERT INTO public.article_tag (article_id, tag_id) VALUES (27, 6);
INSERT INTO public.article_tag (article_id, tag_id) VALUES (2, 1);
INSERT INTO public.article_tag (article_id, tag_id) VALUES (2, 2);
INSERT INTO public.article_tag (article_id, tag_id) VALUES (19, 1);
INSERT INTO public.article_tag (article_id, tag_id) VALUES (19, 4);
INSERT INTO public.article_tag (article_id, tag_id) VALUES (19, 6);


--
-- TOC entry 2863 (class 0 OID 35708)
-- Dependencies: 199
-- Data for Name: blocked; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2864 (class 0 OID 35714)
-- Dependencies: 200
-- Data for Name: coautors; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.coautors (id, article_id, user_id) VALUES (5, 10, 3);
INSERT INTO public.coautors (id, article_id, user_id) VALUES (6, 11, 3);
INSERT INTO public.coautors (id, article_id, user_id) VALUES (7, 12, 3);
INSERT INTO public.coautors (id, article_id, user_id) VALUES (8, 19, 2);
INSERT INTO public.coautors (id, article_id, user_id) VALUES (14, 27, 2);
INSERT INTO public.coautors (id, article_id, user_id) VALUES (17, 25, 3);


--
-- TOC entry 2867 (class 0 OID 35722)
-- Dependencies: 203
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.roles (name, can_edit_roles, can_block_users, can_edit_personnel, can_block_article, can_edit_tag, id) VALUES ('admin', true, true, true, true, true, 1);
INSERT INTO public.roles (name, can_edit_roles, can_block_users, can_edit_personnel, can_block_article, can_edit_tag, id) VALUES ('testRole', false, true, false, false, false, 2);


--
-- TOC entry 2866 (class 0 OID 35719)
-- Dependencies: 202
-- Data for Name: personnel; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.personnel (role_id, user_id) VALUES (1, 1);
INSERT INTO public.personnel (role_id, user_id) VALUES (2, 2);


--
-- TOC entry 2877 (class 0 OID 0)
-- Dependencies: 198
-- Name: articles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.articles_id_seq', 27, true);


--
-- TOC entry 2878 (class 0 OID 0)
-- Dependencies: 201
-- Name: coautors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.coautors_id_seq', 17, true);


--
-- TOC entry 2879 (class 0 OID 0)
-- Dependencies: 204
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 1, false);


--
-- TOC entry 2880 (class 0 OID 0)
-- Dependencies: 207
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


-- Completed on 2018-07-17 15:40:11

--
-- PostgreSQL database dump complete
--

