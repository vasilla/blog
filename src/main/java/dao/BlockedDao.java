package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BlockedDao {

    public void deleteBlocked(String blockedId) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("DELETE  FROM blocked WHERE id = ?")) {
                statement.setString(1, blockedId);
                statement.execute();
            }
        }
    }

    public void addBlocked(String blockedId,String reason,long userId) throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("INSERT  INTO blocked " +
                    "(id,reason,user_id) VALUES (?,?,?)")) {
                statement.setString(1, blockedId);
                statement.setString(2, reason);
                statement.setLong(3, userId);
                statement.execute();
            }
        }
    }

    public String getReason(String blockedId)throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT reason FROM blocked WHERE id = ?")) {
                statement.setString(1, blockedId);
                ResultSet resultSet = statement.executeQuery();
                String reason = "";
                if (resultSet.next()) {
                    reason = resultSet.getString(1);
                }
                return reason;
            }
        }
    }
}
