package dao;

import model.Article;
import model.User;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ArticleDao {

    private List<Article> getFromResultSet(ResultSet resultSet) throws SQLException{
        List<Article> articles = new ArrayList<Article>();
        Article article;
        int authorId;
        String authorUsername;
        while (resultSet.next()){
            article = new Article();
            article.setId(resultSet.getLong("id"));
            article.setTitle(resultSet.getString("title"));
            article.setDescription(resultSet.getString("description"));
            article.setPublished(resultSet.getBoolean("published"));
            article.setPublishedDate(resultSet.getTimestamp("published_date"));
            article.setLastUpdated(resultSet.getDate("last_updated"));
            authorId = resultSet.getInt("user_id");
            authorUsername = resultSet.getString("username");
            article.setImageName(resultSet.getString("image_name"));
            article.setAuthor(new User(authorId,authorUsername));
            article.setImageNameFromUrl(article.getImageName().
                    matches("^[a-z]{4,5}:\\/\\/[\\w,.,-,\\/,-]+((\\.jpg)|(\\.png))$"));
            articles.add(article);
        }
        return articles;
    }

    public List<Article> getPublished(long page,int articlesPerPage) throws SQLException {
        List<Article> articles = new ArrayList<>();
        try(Connection connection = DBConnection.getInstance().connect()) {
            ResultSet resultSet;
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT articles.id,title,description,published," +
                    "published_date,last_updated,user_id,image_name,users.username FROM articles" +
                    " INNER JOIN users ON  articles.user_id = users.id WHERE published = 'true' AND articles.blocked_id IS NULL  " +
                    "ORDER BY published_date DESC ,articles.id DESC OFFSET ?  ROWS LIMIT ?")) {
                preparedStatement.setLong(1, (page - 1) * articlesPerPage);
                preparedStatement.setInt(2, articlesPerPage);
                resultSet = preparedStatement.executeQuery();
                return getFromResultSet(resultSet);
            }
        }

    }
    public int getCountOfPublished() throws SQLException{


        try (Connection connection = DBConnection.getInstance().connect()) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("SELECT count(articles.id) from articles " +
                        "where published = 'true' AND articles.blocked_id IS NULL ");
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else throw new MyDatabaseException("Something wrong with  getCountOfPublished  in ArticleDao");
            }

        }

    }

    public List<Article> getMy(long userId,long page,int articlesPerPage) throws SQLException {
        List<Article> articles = new ArrayList<>();
        try(Connection connection = DBConnection.getInstance().connect()) {
            ResultSet resultSet;
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT users.username,aricledata.* FROM users INNER JOIN( SELECT articles.id,title,description,published,published_date,last_updated,user_id,image_name,(articles.blocked_id IS NULL) as blocked_bool FROM articles " +
                    "  INNER JOIN (SELECT coautors.article_id AS a_id,user_id AS user_requested from coautors where user_id = ?) AS couauthorstemp ON articles.id = a_id UNION " +
                    "SELECT articles.id,title,description,published,published_date,last_updated,user_id,image_name,(articles.blocked_id IS NULL) as blocked_bool  FROM articles WHERE user_id = ?) AS aricledata ON users.id = aricledata.user_id ORDER BY aricledata.last_updated DESC ,aricledata.id DESC OFFSET ?  ROWS LIMIT ?;")) {
                preparedStatement.setLong(1, userId);
                preparedStatement.setLong(2, userId);
                preparedStatement.setLong(3, (page - 1) * articlesPerPage);
                preparedStatement.setInt(4, articlesPerPage);
                resultSet = preparedStatement.executeQuery();
                articles = getFromResultSet(resultSet);
            }

        }
        return articles;
    }
    public int getCountOfMy(long userId) throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {

            try(PreparedStatement preparedStatement = connection.prepareStatement("select sum(count_table.article_count) from (select count(id)" +
                    " as article_count from articles where user_id = ? union select count(id) as article_count " +
                    "from coautors where user_id = ?) as count_table")) {
                preparedStatement.setLong(1, userId);
                preparedStatement.setLong(2, userId);
                ResultSet resultSet = preparedStatement.executeQuery();

                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
                else throw new MyDatabaseException("Something wrong with  getCountOfMy  in ArticleDao");
            }
        }

    }

    public String getHtmlById(long articleId) throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try(PreparedStatement statement = connection.prepareStatement("SELECT html FROM articles WHERE articles.id = ?" +
                    " AND html IS NOT NULL")) {
                statement.setLong(1, articleId);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getString(1);
                }
                else return "";
            }
        }

    }

    public Article getById(long articleId) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT articles.id,title,description,published,published_date,last_updated,user_id,image_name,articles.blocked_id,users.username FROM articles " +
                    "inner join users  on articles.user_id = users.id " +
                    "WHERE articles.id = ?")) {
                statement.setLong(1, articleId);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    Article article = new Article();
                    article.setId(resultSet.getLong("id"));
                    article.setTitle(resultSet.getString("title"));
                    article.setDescription(resultSet.getString("description"));
                    article.setPublished(resultSet.getBoolean("published"));
                    article.setPublishedDate(resultSet.getTimestamp("published_date"));
                    article.setLastUpdated(resultSet.getDate("last_updated"));
                    article.setAuthor(new User(resultSet.getInt("user_id"), resultSet.getString("username")));
                    article.setImageName(resultSet.getString("image_name"));
                    article.setBlockedId(resultSet.getString("blocked_id"));
                    article.setImageNameFromUrl(article.getImageName().
                            matches("^[a-z]{4,5}:\\/\\/[\\w,.,-,\\/,-]+((\\.jpg)|(\\.png))$"));
                    return article;

                }
                throw new MyDatabaseException("Something wrong with  getById  in ArticleDao");
            }
        }

    }

    public void update(Article article) throws SQLException{

        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("UPDATE articles SET " +
                    "title = ?,description = ?,published = ?,published_date = ?,last_updated = ?,image_name = ? " +
                    "WHERE id = ?")) {
                statement.setString(1, article.getTitle());
                statement.setString(2, article.getDescription());
                statement.setBoolean(3, article.isPublished());
                statement.setTimestamp(4, article.getPublishedDate());
                statement.setDate(5, article.getLastUpdated());
                statement.setString(6, article.getImageName());
                statement.setLong(7, article.getId());
                statement.execute();

            }
        }
    }

    public void publish(long articleId) throws SQLException{

        try(Connection connection = DBConnection.getInstance().connect()){
            try(PreparedStatement statement = connection.prepareStatement("UPDATE articles SET " +
                "published = ?,published_date = ?,last_updated = ? " +
                "WHERE id = ?")) {
                Date date = new Date(Calendar.getInstance().getTimeInMillis());

                statement.setBoolean(1, true);
                statement.setTimestamp(2, Timestamp.from(Instant.now()));
                statement.setDate(3, date);
                statement.setLong(4, articleId);
                statement.execute();
            }
        }
    }

    public void delete(long articleId) throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("DELETE  FROM articles WHERE id = ?")) {
                statement.setLong(1, articleId);
                statement.execute();
            }
        }
    }

    public void create(Article article) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("INSERT  INTO articles " +
                    "(title,description,published,published_date,last_updated,user_id,image_name) VALUES (?,?,?,?,?,?,?)")) {
                statement.setString(1, article.getTitle());
                statement.setString(2, article.getDescription());
                statement.setBoolean(3, article.isPublished());
                statement.setTimestamp(4, article.getPublishedDate());
                statement.setDate(5, article.getLastUpdated());
                statement.setLong(6, article.getAuthor().getId());
                statement.setString(7, article.getImageName());
                statement.execute();
            }
        }
    }

    public boolean canEdit(long userId,long articleId) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT user_id FROM articles" +
                    " WHERE articles.id = ? AND articles.user_id = ? UNION SELECT user_id FROM coautors " +
                    "WHERE coautors.article_id = ? AND coautors.user_id = ?")) {
                statement.setLong(1, articleId);
                statement.setLong(2, userId);
                statement.setLong(3, articleId);
                statement.setLong(4, userId);
                ResultSet resultSet = statement.executeQuery();
                return resultSet.next();
            }
        }
    }
    public boolean canDelete(long userId,long articleId) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT user_id FROM articles WHERE" +
                    " articles.id = ? AND articles.user_id = ? UNION select user_id from personnel inner join roles on" +
                    " personnel.role_id=roles.id where user_id=? and  roles.can_edit_roles = 'true'")) {
                statement.setLong(1, articleId);
                statement.setLong(2, userId);
                statement.setLong(3, userId);
                ResultSet resultSet = statement.executeQuery();
                return resultSet.next();
            }
        }
    }
    public void updateContent(long articleId,String contentHtml) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("UPDATE articles SET " +
                    "html = ?,last_updated = ? " +
                    "WHERE id = ?")) {
                Date date = new Date(Calendar.getInstance().getTimeInMillis());
                statement.setString(1, contentHtml);
                statement.setDate(2, date);
                statement.setLong(3, articleId);
                statement.execute();
            }
        }
    }
    public boolean checkIfExists(long articleId,long userId) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT id FROM coautors WHERE article_id =? " +
                    "AND coautors.user_id= ?")) {
                statement.setLong(1, articleId);
                statement.setLong(2, userId);
                ResultSet resultSet = statement.executeQuery();
                return resultSet.next();
            }
        }
    }

    public void addCoauthor(long articleId,long userId) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("INSERT INTO coautors(article_id, user_id) VALUES (?,?)")) {
                statement.setLong(1, articleId);
                statement.setLong(2, userId);
                statement.execute();
            }
        }
    }
    public void deleteAllCoauthor(long articleId) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("DELETE FROM coautors WHERE article_id = ?")) {
                statement.setLong(1, articleId);
                statement.execute();
            }
        }
    }
    public void deleteCoauthor(long articleId,long userId) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("DELETE FROM coautors WHERE article_id = ? AND " +
                    "user_id = ?")) {
                statement.setLong(1, articleId);
                statement.setLong(2, userId);
                statement.execute();
            }
        }
    }

    public List<User> getCoauthor(long articleId) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT users.id,users.username FROM users " +
                    "INNER JOIN coautors ON users.id = coautors.user_id WHERE coautors.article_id = ?")) {
                statement.setLong(1, articleId);
                ResultSet resultSet = statement.executeQuery();
                List<User> users = new ArrayList<>();
                while (resultSet.next()) {
                    users.add(new User(resultSet.getLong("id"), resultSet.getString("username")));
                }
                return users;
            }
        }
    }
    public List<User> getPossibleCoauthor(long articleId,String usernamePart,int limit) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT users.id,users.username FROM users " +
                    "INNER JOIN (SELECT users.id FROM users EXCEPT (SELECT user_id FROM articles WHERE id = ? UNION SELECT " +
                    "user_id FROM coautors WHERE article_id = ?)) AS posible_users  ON users.id =posible_users.id WHERE " +
                    "username LIKE  ? LIMIT ? ")) {
                statement.setLong(1, articleId);
                statement.setLong(2, articleId);
                statement.setString(3, "%" + usernamePart + "%");
                statement.setInt(4, limit);
                ResultSet resultSet = statement.executeQuery();
                List<User> users = new ArrayList<>();
                while (resultSet.next()) {
                    users.add(new User(resultSet.getLong("id"), resultSet.getString("username")));
                }
                return users;
            }
        }
    }
    public List<Article> searchByTitleMinimum(String titlePart,int limit) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT id,title FROM articles " +
                    "WHERE published = 'true' AND articles.blocked_id IS NULL AND title LIKE ? ORDER BY published_date DESC ,id DESC  LIMIT ?")) {
                statement.setString(1, "%" + titlePart + "%");
                statement.setInt(2, limit);
                ResultSet resultSet = statement.executeQuery();
                List<Article> articles = new ArrayList<>();
                Article article;
                while (resultSet.next()) {
                    article = new Article();
                    article.setId(resultSet.getLong("id"));
                    article.setTitle(resultSet.getString("title"));
                    articles.add(article);
                }
                return articles;
            }
        }
    }

    public List<Article> searchByTitle(String titlePart,long page,int articlesPerPage) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT articles.id,title,description,published," +
                    "published_date,last_updated,user_id,image_name,users.username FROM articles" +
                    " INNER JOIN users ON  articles.user_id = users.id WHERE published = 'true'  AND articles.blocked_id IS NULL AND articles.title LIKE ? " +
                    "ORDER BY published_date DESC ,articles.id DESC OFFSET ?  ROWS LIMIT ?")) {
                preparedStatement.setString(1, "%" + titlePart + "%");
                preparedStatement.setLong(2, (page - 1) * articlesPerPage);
                preparedStatement.setInt(3, articlesPerPage);
                ResultSet resultSet = preparedStatement.executeQuery();
                return getFromResultSet(resultSet);
            }
        }
    }

    public int getCountOfSearchByTitle(String titlePart) throws SQLException {
        try (Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT (articles.id) FROM articles" +
                    " WHERE published = 'true' AND articles.blocked_id IS NULL AND articles.title LIKE ? ")) {
                preparedStatement.setString(1, "%" + titlePart + "%");
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
                throw new MyDatabaseException("something goes wrong");
            }
        }
    }
    public List<Article> searchByTitleForAdmin(String titlePart,int page,int articlesPerPage) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT id,title,(articles.blocked_id IS NOT NULL) as blocked_bool,user_id,published_date,users.username FROM articles " +
                    " INNER JOIN users ON articles.user_id = users.id WHERE published = 'true'  AND title LIKE ? ORDER BY published_date DESC ,id DESC  OFFSET ?  ROWS LIMIT ?")) {
                statement.setString(1, "%" + titlePart + "%");
                statement.setInt(2, page);
                statement.setInt(3, articlesPerPage);
                ResultSet resultSet = statement.executeQuery();
                List<Article> articles = new ArrayList<>();
                Article article;
                while (resultSet.next()) {
                    article = new Article();
                    article.setId(resultSet.getLong("id"));
                    article.setTitle(resultSet.getString("title"));
                    article.setBlocked(resultSet.getBoolean("blocked_bool"));
                    article.setAuthor(new User(resultSet.getInt("user_id"), resultSet.getString("username")));
                    article.setPublishedDate(resultSet.getTimestamp("published_date"));
                    articles.add(article);
                }
                return articles;
            }
        }
    }
    public List<Article> getAllForAdmin(int page,int articlesPerPage) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT articles.id,title,(articles.blocked_id IS NOT NULL) as blocked_bool,user_id,published_date,users.username FROM articles " +
                    " INNER JOIN users ON articles.user_id = users.id WHERE published = 'true'   ORDER BY published_date DESC ,id DESC  OFFSET ?  ROWS LIMIT ?")) {

                statement.setInt(1, (page - 1) * articlesPerPage);
                statement.setInt(2, articlesPerPage);
                ResultSet resultSet = statement.executeQuery();
                List<Article> articles = new ArrayList<>();
                Article article;
                while (resultSet.next()) {
                    article = new Article();
                    article.setId(resultSet.getLong("id"));
                    article.setTitle(resultSet.getString("title"));
                    article.setBlocked(resultSet.getBoolean("blocked_bool"));
                    article.setAuthor(new User(resultSet.getInt("user_id"), resultSet.getString("username")));
                    article.setPublishedDate(resultSet.getTimestamp("published_date"));
                    articles.add(article);
                }
                return articles;
            }
        }
    }
    public int getCountOfSearchByTitleForAdmin(String titlePart) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT (articles.id) FROM articles" +
                    " WHERE published = 'true' AND  AND articles.title LIKE ? ")) {
                preparedStatement.setString(1, "%" + titlePart + "%");
                ResultSet resultSet = preparedStatement.executeQuery();

                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
                throw new MyDatabaseException("something goes wrong in getCountOfSearchByTitleForAdmin");
            }
        }
    }
    public int getCountOfPublishedForAdmin() throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("SELECT COUNT (articles.id) FROM articles" +
                        " WHERE published = 'true' ");
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
                throw new MyDatabaseException("something goes wrong in getCountOfPublishedForAdmin");
            }
        }
    }



    public void setBlocked(long articleId,String blockedId) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("UPDATE articles SET " +
                    "blocked_id = ? " +
                    "WHERE id = ?")) {
                statement.setString(1, blockedId);
                statement.setLong(2, articleId);
                statement.execute();
            }
        }

    }
    public void setUnblocked(long articleId) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("UPDATE articles SET " +
                    "blocked_id = ? " +
                    "WHERE id = ?")) {
                statement.setNull(1, Types.VARCHAR);
                statement.setLong(2, articleId);
                statement.execute();
            }
        }
    }





}
