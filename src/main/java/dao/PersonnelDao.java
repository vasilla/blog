package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonnelDao {
    public void delete(long userId)throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM personnel " +
                    "WHERE user_id = ?")) {
                preparedStatement.setLong(1, userId);
                preparedStatement.execute();
            }
        }

    }
    public void update(long userId,long roleId)throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE personnel" +
                    " SET role_id = ? WHERE user_id = ?");) {
                preparedStatement.setLong(1, roleId);
                preparedStatement.setLong(2, userId);
                preparedStatement.execute();
            }
        }

    }
    public void insert(long userId,long roleId)throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("" +
                    "INSERT INTO personnel(role_id, user_id) VALUES (?,?)")) {
                preparedStatement.setLong(1, roleId);
                preparedStatement.setLong(2, userId);
                preparedStatement.execute();
            }
        }

    }
    public boolean checkIfExist(long userId)throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM personnel " +
                    "WHERE user_id = ?")) {
                preparedStatement.setLong(1, userId);
                ResultSet resultSet = preparedStatement.executeQuery();
                return  resultSet.next();
            }
        }
    }
}
