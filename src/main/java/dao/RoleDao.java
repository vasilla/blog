package dao;

import model.Role;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RoleDao {





    public void delete(long roleId) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("DELETE  FROM roles WHERE id = ?")) {
                statement.setLong(1, roleId);
                statement.execute();
            }
        }
    }
    public void add(Role role) throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("INSERT  INTO roles " +
                    "(name, can_edit_roles, can_block_users, can_edit_personnel, can_block_article, can_edit_tag)" +
                    " VALUES (?,?,?,?,?,?)")) {
                statement.setString(1, role.getName());
                statement.setBoolean(2, role.isCanEditRoles());
                statement.setBoolean(3, role.isCanBlockUsers());
                statement.setBoolean(4, role.isCanEditPersonnel());
                statement.setBoolean(5, role.isCanBlockArticle());
                statement.setBoolean(6, role.isCanEditTag());
                statement.execute();
            }
        }
    }
    public void update(Role role) throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("UPDATE roles " +
                    "SET name=?,can_edit_roles=?, can_block_users=?, can_edit_personnel=?, can_block_article=?," +
                    " can_edit_tag=? WHERE id = ?")) {
                statement.setString(1, role.getName());
                statement.setBoolean(2, role.isCanEditRoles());
                statement.setBoolean(3, role.isCanBlockUsers());
                statement.setBoolean(4, role.isCanEditPersonnel());
                statement.setBoolean(5, role.isCanBlockArticle());
                statement.setBoolean(6, role.isCanEditTag());
                statement.setLong(7, role.getId());
                statement.execute();
            }
        }
    }
    public List<Role> getForAdmin(int page, int maxPerPage) throws SQLException{
        List<Role> roles = new ArrayList<>();
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM roles order by id desc OFFSET ?  ROWS LIMIT ?")){
                statement.setInt(1, (page - 1) * maxPerPage);
                statement.setInt(2, maxPerPage);
                ResultSet resultSet = statement.executeQuery();
                Role role;
                while (resultSet.next()) {
                    role = new Role();
                    role.setId(resultSet.getLong("id"));
                    role.setName(resultSet.getString("name"));
                    role.setCanBlockArticle(resultSet.getBoolean("can_block_article"));
                    role.setCanBlockUsers(resultSet.getBoolean("can_block_users"));
                    role.setCanEditTag(resultSet.getBoolean("can_edit_tag"));
                    role.setCanEditRoles(resultSet.getBoolean("can_edit_roles"));
                    role.setCanEditPersonnel(resultSet.getBoolean("can_edit_personnel"));
                    roles.add(role);
                }
                return roles;
            }
        }
    }
    public int getCount() throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("SELECT count(id) from roles");
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else throw new MyDatabaseException("Something goes wrong in getCountOfUsers");
            }
        }
    }
    public List<Role> getRoleNames() throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("SELECT id,name FROM roles");
                List<Role> roles = new ArrayList<>();
                roles.add(new Role(-1, "user"));
                while (resultSet.next()) {
                    roles.add(new Role(resultSet.getLong("id"), resultSet.getString("name")));
                }
                return roles;
            }
        }
    }
}
