package dao;


import model.Blocked;
import model.Role;
import model.User;
import utils.AuthorizationToken;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    private User getFromResultSet(ResultSet resultSet)throws SQLException{
        if (resultSet.next()){
            User user = new User();
            user.setId(resultSet.getInt("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            user.setUsername(resultSet.getString("username"));
            return user;
        }
        throw new MyDatabaseException("User not found");
    }

    public User getByUsername(String username) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE " +
                    "username = ?")) {
                preparedStatement.setString(1, username);
                return getFromResultSet(preparedStatement.executeQuery());
            }
        }
    }
    public User getByLogin(String login) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE " +
                    "login = ?")) {
                preparedStatement.setString(1, login);
                return getFromResultSet(preparedStatement.executeQuery());
            }
        }
    }

    public List<Integer> getUserRights(long userId) throws SQLException {
        List<Integer> rights = new ArrayList<>();
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT roles.can_block_article," +
                    "roles.can_block_users,roles.can_edit_tag,roles.can_edit_roles,roles.can_edit_personnel " +
                    "from personnel inner join roles on role_id = roles.id where personnel.user_id = ?")) {
                preparedStatement.setLong(1, userId);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    if (resultSet.getBoolean("can_block_article")) {
                        rights.add(AuthorizationToken.CAN_BLOCK_ARTICLE);
                    }
                    if (resultSet.getBoolean("can_block_users")) {
                        rights.add(AuthorizationToken.CAN_BLOCK_USER);
                    }
                    if (resultSet.getBoolean("can_edit_tag")) {
                        rights.add(AuthorizationToken.CAN_EDIT_TAG);
                    }
                    if (resultSet.getBoolean("can_edit_roles")) {
                        rights.add(AuthorizationToken.CAN_EDIT_PERSONNEL);
                    }
                    if (resultSet.getBoolean("can_edit_personnel")) {
                        rights.add(AuthorizationToken.CAN_EDIT_PERSONNEL);
                    }
                }
                return rights;
            }
        }
    }

    public List<User> getForAdmin(int page,int maxPerPage) throws SQLException{
        List<User> users = new ArrayList<>();
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT users.id,users.username,users.blocked_id," +
                    "users.blocked_id IS not  null as blocked_bool,role_temp.name,role_temp.id as role_id,role_temp.has_role" +
                    " from users left join (SELECT roles.id,roles.name,personnel.user_id,role_id is not null  as has_role" +
                    " from roles inner join personnel on" +
                    " roles.id = personnel.role_id) as role_temp on role_temp.user_id = users.id ORDER BY users.id OFFSET ?  ROWS LIMIT ?")) {
                statement.setInt(1, (page - 1) * maxPerPage);
                statement.setInt(2, maxPerPage);
                ResultSet resultSet = statement.executeQuery();
                User user;
                while (resultSet.next()) {
                    user = new User();
                    user.setId(resultSet.getLong("id"));
                    user.setUsername(resultSet.getString("username"));
                    if (resultSet.getBoolean("blocked_bool")) {
                        user.setBlocked(new Blocked(resultSet.getString("blocked_id")));
                    } else {
                        user.setBlocked(null);
                    }
                    if (resultSet.getBoolean("has_role")) {
                        user.setRole(new Role(resultSet.getLong("role_id"), resultSet.getString("name")));
                    } else user.setRole(new Role());
                    users.add(user);
                }
                return users;
            }
        }
    }
    public int getCountOfUsers() throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("SELECT count(id) from users");
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else throw new MyDatabaseException("Something goes wrong in getCountOfUsers");
            }
        }
    }

    public void setBlocked(long userId,String blockedId) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("UPDATE users SET " +
                    "blocked_id = ? " +
                    "WHERE id = ?")){
                 statement.setString(1, blockedId);
                    statement.setLong(2, userId);
                    statement.execute();

            }
        }
    }
    public void setUnblocked(long userId) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("UPDATE users SET " +
                    "blocked_id = ? " +
                    "WHERE id = ?")) {
                statement.setNull(1, Types.VARCHAR);
                statement.setLong(2, userId);
                statement.execute();
            }
        }
    }
    public String getBlockedId(long userId) throws SQLException {
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT blocked_id FROM users WHERE id = ?")) {
                statement.setLong(1, userId);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    String blochedId = resultSet.getString(1);
                    connection.close();
                    return blochedId;
                }
                throw new MyDatabaseException("getBlockedId Exeption");
            }
        }

    }




}
