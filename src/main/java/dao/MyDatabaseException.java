package dao;

import java.sql.SQLException;

public class MyDatabaseException extends SQLException {
    public MyDatabaseException(String reason) {
        super(reason);
    }
}
