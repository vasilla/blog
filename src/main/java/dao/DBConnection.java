package dao;

import utils.PropertiesHolder;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {

    private static DBConnection instance;
    private Logger log;


    public DBConnection() {
        log = Logger.getLogger(getClass().getName());
    }


    public Connection connect(){
        Connection connection;
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println(PropertiesHolder.getInstance().getDatabaseName());
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/"+
                            PropertiesHolder.getInstance().getDatabaseName(),
                    PropertiesHolder.getInstance().getDatabaseUser(),
                    PropertiesHolder.getInstance().getDatabasePassword());

        }catch (SQLException e){
            log.warning(e.getMessage());
            connection = null;
        }
        catch (ClassNotFoundException e2){
            log.warning("Class.forName exception."+e2.getMessage());
            connection = null;
        }
        return connection;
    }



    public static DBConnection getInstance(){
        if (instance == null) instance = new DBConnection();
        return instance;
    }



}
