package dao;

import model.Tag;
import model.User;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TagDao {

   public List<Tag> getAllWithoutUser() throws SQLException {
       try(Connection connection = DBConnection.getInstance().connect()) {
           try (Statement statement = connection.createStatement()) {
               ResultSet resultSet = statement.executeQuery("SELECT id,name FROM tags");
               List<Tag> tags = new ArrayList<>();
               while (resultSet.next()) {
                   tags.add(new Tag(resultSet.getLong("id"), resultSet.getString("name")));
               }
               return tags;
           }
       }
   }
   public List<Tag> getForArticle(long articleId) throws SQLException {
       try(Connection connection = DBConnection.getInstance().connect()) {
           try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT tags.id,tags.name," +
                   "(article_id IS NOT NULL ) AS selected FROM tags LEFT JOIN  article_tag  ON tags.id = article_tag.tag_id" +
                   " AND article_tag.article_id = ?")) {
               preparedStatement.setLong(1, articleId);
               ResultSet resultSet = preparedStatement.executeQuery();
               List<Tag> tags = new ArrayList<>();
               while (resultSet.next()) {
                   tags.add(new Tag(resultSet.getLong("id"), resultSet.getString("name"),
                           resultSet.getBoolean("selected")));
               }
               return tags;
           }
       }
   }

   public void deleteArticleTag(long articleId) throws SQLException {
       try(Connection connection = DBConnection.getInstance().connect()) {
           try (PreparedStatement statement = connection.prepareStatement("DELETE  FROM article_tag WHERE article_id = ?")) {
               statement.setLong(1, articleId);
               statement.execute();
           }
       }
   }


    public void addArticleTag(long articleId,long tagId) throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("INSERT  INTO article_tag " +
                    "(article_id,tag_id) VALUES (?,?)")) {
                statement.setLong(1, articleId);
                statement.setLong(2, tagId);
                statement.execute();
            }
        }
    }

   public void delete(long tagId) throws SQLException {
       try(Connection connection = DBConnection.getInstance().connect()) {
           try (PreparedStatement statement = connection.prepareStatement("DELETE  FROM tags WHERE id = ?")) {
               statement.setLong(1, tagId);
               statement.execute();
           }
       }
   }
    public void add(String name,long userId) throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("INSERT  INTO tags " +
                    "(user_id, name) VALUES (?,?)")) {
                statement.setLong(1, userId);
                statement.setString(2, name);
                statement.execute();
            }
        }
    }
    public void update(long tagId,String name,long userId) throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("UPDATE tags " +
                    "SET user_id = ?,name = ? where id = ?")) {
                statement.setLong(1, userId);
                statement.setString(2, name);
                statement.setLong(3, tagId);
                statement.execute();
            }
        }
    }
    public List<Tag> getForAdmin(int page,int maxPerPage) throws SQLException{
        List<Tag> tags = new ArrayList<>();
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT tags.*,users.username from tags " +
                    "inner join users on tags.user_id = users.id order by id desc OFFSET ?  ROWS LIMIT ?")) {
                statement.setInt(1, (page - 1) * maxPerPage);
                statement.setInt(2, maxPerPage);
                ResultSet resultSet = statement.executeQuery();
                Tag tag;
                while (resultSet.next()) {
                    tag = new Tag();
                    tag.setId(resultSet.getLong("id"));
                    tag.setName(resultSet.getString("name"));
                    tag.setAuthor(new User(resultSet.getLong("user_id"), resultSet.getString("username")));
                    tags.add(tag);
                }
                return tags;
            }
        }
    }
    public int getCount() throws SQLException{
        try(Connection connection = DBConnection.getInstance().connect()) {
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("SELECT count(id) from tags");
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else throw new MyDatabaseException("Something goes wrong in getCountOfUsers");
            }
        }
    }

}
