package model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


public class User {
    @NotNull
    @Min(1)
    private long id;
    @NotNull
    @Min(8)
    @Max(30)
    private String login;
    @NotNull
    @Min(8)
    @Max(30)
    private String password;
    @NotNull
    @Min(8)
    @Max(30)
    private String username;
    private Blocked blocked;

    public boolean isBlocked(){
        return (blocked != null);
    }
    public Blocked getBlocked() {
        return blocked;
    }

    public void setBlocked(Blocked blocked) {
        this.blocked = blocked;
    }

    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User() {

    }


    public User(long id, String username) {
        this.id = id;
        this.username = username;
    }

    public User(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
