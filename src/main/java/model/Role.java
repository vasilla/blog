package model;

public class Role {
    private long id;
    private String name;
    private boolean canEditRoles;
    private boolean canBlockUsers;
    private boolean canEditPersonnel;
    private boolean canBlockArticle;
    private boolean canEditTag;


    public Role() {
        name = "user";
        id = -1;
    }

    public Role(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Role(String name, boolean canEditRoles, boolean canBlockUsers, boolean canEditPersonnel,
                boolean canBlockArticle, boolean canEditTag) {
        this.name = name;
        this.canEditRoles = canEditRoles;
        this.canBlockUsers = canBlockUsers;
        this.canEditPersonnel = canEditPersonnel;
        this.canBlockArticle = canBlockArticle;
        this.canEditTag = canEditTag;
    }

    public boolean isCanEditRoles() {
        return canEditRoles;
    }

    public void setCanEditRoles(boolean canEditRoles) {
        this.canEditRoles = canEditRoles;
    }

    public boolean isCanBlockUsers() {
        return canBlockUsers;
    }

    public void setCanBlockUsers(boolean canBlockUsers) {
        this.canBlockUsers = canBlockUsers;
    }

    public boolean isCanEditPersonnel() {
        return canEditPersonnel;
    }

    public void setCanEditPersonnel(boolean canEditPersonnel) {
        this.canEditPersonnel = canEditPersonnel;
    }

    public boolean isCanBlockArticle() {
        return canBlockArticle;
    }

    public void setCanBlockArticle(boolean canBlockArticle) {
        this.canBlockArticle = canBlockArticle;
    }

    public boolean isCanEditTag() {
        return canEditTag;
    }

    public void setCanEditTag(boolean canEditTag) {
        this.canEditTag = canEditTag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
