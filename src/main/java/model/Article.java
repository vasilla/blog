package model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.sql.Timestamp;

public class Article {
    @NotNull
    @Min(1)
    private long id;
    @NotNull
    @Min(1)
    @Max(60)
    private String title;
    @NotNull
    @Min(1)
    @Max(150)
    private String description;
    @NotNull
    private boolean published;
    private Timestamp publishedDate;
    @NotNull
    private Date lastUpdated;
    private User author;
    private String imageName;
    private boolean imageNameFromUrl;
    private boolean blocked;
    private String blockedId;

    public String getBlockedId() {
        return blockedId;
    }

    public void setBlockedId(String blockedId) {
        this.blockedId = blockedId;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isImageNameFromUrl() {
        return imageNameFromUrl;
    }

    public void setImageNameFromUrl(boolean imageNameFromUrl) {
        this.imageNameFromUrl = imageNameFromUrl;
    }

    public Article() {
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public Timestamp getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Timestamp publishedDate) {
        this.publishedDate = publishedDate;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
