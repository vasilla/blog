package filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

public class MyFilter implements Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("MyFilter init");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        Enumeration<String> attributeNames = httpServletRequest.getAttributeNames();
        System.out.println("attributeNames----------------");
        while (attributeNames.hasMoreElements()){
            String el = attributeNames.nextElement();
            System.out.println(el+":"+httpServletRequest.getAttribute(el));
        }

        if ((boolean)httpServletRequest.getAttribute("logged")) {
            chain.doFilter(httpServletRequest, httpServletResponse);
        }
        else {
            request.setAttribute("errorMessage","Youd don`t have access to this page.");
            request.getRequestDispatcher("/jsp/error.jsp").forward(request,response);
            //TODO write logs
        }


    }

    @Override
    public void destroy() {
        System.out.println("MyFilter init");
    }
}
