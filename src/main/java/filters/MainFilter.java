package filters;

import utils.AuthorizationToken;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.GeneralSecurityException;

public class MainFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("MainFilter init");
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        if (request.getSession(false) == null){
            System.out.println("Session null");

        }
        Object token =  request.getSession().getAttribute("token");

        if (token == null){
            req.setAttribute("logged",false);
            System.out.println("token NULL");
        }
        else {
            try {
                AuthorizationToken authorizationToken = new AuthorizationToken((String) token);
                request.setAttribute("logged",true);
                request.setAttribute("username",authorizationToken.getUsername());
                request.setAttribute("userId",authorizationToken.getUserId());
                boolean hasRights = authorizationToken.hasRights();
                request.setAttribute("admin_panel_enabled",hasRights);
                if (hasRights){
                    request.setAttribute("can_block_article",authorizationToken.checkRight(AuthorizationToken.CAN_BLOCK_ARTICLE));
                    request.setAttribute("can_block_users",authorizationToken.checkRight(AuthorizationToken.CAN_BLOCK_ARTICLE));
                    request.setAttribute("can_edit_personnel",authorizationToken.checkRight(AuthorizationToken.CAN_EDIT_PERSONNEL));
                    request.setAttribute("can_edit_tags",authorizationToken.checkRight(AuthorizationToken.CAN_EDIT_TAG));
                    request.setAttribute("can_edit_roles",authorizationToken.checkRight(AuthorizationToken.CAN_EDIT_ROLES));
                }
                chain.doFilter(request,response);
            }catch (GeneralSecurityException e){
                request.getSession().removeAttribute("token");
                request.setAttribute("errorMessage","Problem with security token.Please log in again.");
                request.getRequestDispatcher("/jsp/error.jsp").forward(request,response);
                //TODO write to logs

            }
        }
//        chain.doFilter(request,response);


    }

    @Override
    public void destroy() {
        System.out.println("MainFilter destror");
    }
}
