package filters;

import dao.ArticleDao;
import model.Article;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ArticleAuthorFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String articleIdString = request.getParameter("article");
        try {
            ArticleDao articleDao = new ArticleDao();
            long userId = (long)request.getAttribute("userId");
            long articleId = Long.parseLong(articleIdString);
            Article article = articleDao.getById(articleId);
            if (article.getAuthor().getId() == userId){
                chain.doFilter(request,response);
            }
            else throw new Exception("Access not allowed");
        }catch (Exception e){
            request.setAttribute("errorMessage","Youd don`t have access to this page.");
            request.getRequestDispatcher("/jsp/error.jsp").forward(request,response);
        }
    }

    @Override
    public void destroy() {

    }
}
