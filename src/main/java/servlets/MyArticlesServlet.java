package servlets;

import model.Article;
import service.ArticleService;
import utils.AuthorizationToken;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;


//TODO edit add image
public class MyArticlesServlet extends ArticleServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            handleAuthorization(request,response);
            myFilter(request,response);
            long userId = getUserId(request);
            ArticleService articleService = new ArticleService();
            List<Article> articles = articleService.getMy(userId,getPage(request,
                    articleService.getCountOfMy(userId)));
            request.setAttribute("articles",articles);
            request.setAttribute("url_prefix","/my");
            request.setAttribute("imagesPath",getImageUrl(request));
            request.getRequestDispatcher("/jsp/my.jsp").forward(request,response);
        }  catch (SQLException e) {
            handleSQLExeption(request,response,e);
        } catch (Exception e) {
            handleExeption(request,response,e);
        }
    }



}
