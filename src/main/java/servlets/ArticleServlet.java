package servlets;


import dao.ArticleDao;
import dao.MyDatabaseException;
import model.Article;
import utils.AuthorizationToken;
import utils.PropertiesHolder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.UUID;

public class ArticleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request,response);
    }

    public void handleAuthorization(HttpServletRequest request,HttpServletResponse response) throws  ServletException, IOException {
        Object token =  request.getSession().getAttribute("token");
        if (token == null){
            request.setAttribute("logged",false);
            return ;
        }
        else {
            try {
                AuthorizationToken authorizationToken = new AuthorizationToken((String) token);
                request.setAttribute("logged", true);
                request.setAttribute("username", authorizationToken.getUsername());
                request.setAttribute("userId", authorizationToken.getUserId());
                request.setAttribute("urlPrefix",request.getContextPath());
                boolean hasRights = authorizationToken.hasRights();
                request.setAttribute("admin_panel_enabled", hasRights);
                if (hasRights) {
                    request.setAttribute("can_block_article", authorizationToken.checkRight(AuthorizationToken.CAN_BLOCK_ARTICLE));
                    request.setAttribute("can_block_users", authorizationToken.checkRight(AuthorizationToken.CAN_BLOCK_ARTICLE));
                    request.setAttribute("can_edit_personnel", authorizationToken.checkRight(AuthorizationToken.CAN_EDIT_PERSONNEL));
                    request.setAttribute("can_edit_tags", authorizationToken.checkRight(AuthorizationToken.CAN_EDIT_TAG));
                    request.setAttribute("can_edit_roles", authorizationToken.checkRight(AuthorizationToken.CAN_EDIT_ROLES));
                }
            }catch (GeneralSecurityException e){
                handleGeneralSecurityException(request,response,e);

            }
        }
    }
    public static void handleGeneralSecurityException(HttpServletRequest request, HttpServletResponse response, GeneralSecurityException e)
            throws ServletException, IOException {
        request.setAttribute("errorMessage","Problem with security token.Please log in again.");
        request.getRequestDispatcher(request.getContextPath()+"/jsp/error.jsp").forward(request,response);

    }
    public static void handleSQLExeption(HttpServletRequest request, HttpServletResponse response, SQLException e)
            throws ServletException, IOException {
        request.setAttribute("errorMessage","Problem with database.");
        request.getRequestDispatcher(request.getContextPath()+"/jsp/error.jsp").forward(request,response);

    }
    public static void handleExeption(HttpServletRequest request, HttpServletResponse response, Exception e) throws
            ServletException, IOException {
        if (!response.isCommitted()) {
            request.setAttribute("errorMessage", "Something unexpected happen.");
            request.getRequestDispatcher(request.getContextPath() + "/jsp/error.jsp").forward(request, response);
        }
    }
    public boolean getlogged(HttpServletRequest request){
        return (boolean) request.getAttribute("logged");
    }
    public long getUserId(HttpServletRequest request){
        return (long) request.getAttribute("userId");
    }
    public String getUserName(HttpServletRequest request){
        return (String) request.getAttribute("username");
    }
    public void myFilter(HttpServletRequest request,HttpServletResponse response) throws ServletException,
            IOException, SQLException {
        if (!getlogged(request)){
            handleAccessDenied(request,response);
        }
    }
    public void editFilter(HttpServletRequest request,HttpServletResponse response) throws ServletException,
            IOException, SQLException {
        String articleIdString = request.getParameter("article");
        ArticleDao articleDao = new ArticleDao();
        long userId = (long)request.getAttribute("userId");
        long articleId = Long.parseLong(articleIdString);
        if (!articleDao.canEdit(userId,articleId)){
           handleAccessDenied(request,response);
        }
    }
    public void authorFilter(HttpServletRequest request,HttpServletResponse response) throws ServletException,
            IOException, SQLException {
        if (!getlogged(request)){
            handleAccessDenied(request,response);
        }
        String articleIdString = request.getParameter("article");
        ArticleDao articleDao = new ArticleDao();
        long userId = getUserId(request);
        long articleId = Long.parseLong(articleIdString);
        Article article = articleDao.getById(articleId);
        if (article.getAuthor().getId() != userId){
            handleAccessDenied(request,response);
        }
    }
    public static void handleAccessDenied(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("errorMessage","You don`t have access to this page.");
        request.getRequestDispatcher(request.getContextPath()+"/jsp/error.jsp").forward(request,response);
    }
    public int getPage(HttpServletRequest request,int elementCount,int maxElements) throws Exception {
        int page;
        long maxPageLong = Math.round(Math.ceil((double) elementCount/maxElements));
        if (maxPageLong > Integer.MAX_VALUE) throw new Exception("Integer not enough in page method:getPage");
        int maxPage =  (int)maxPageLong;
        if (maxPage > 1){
            request.setAttribute("displayPaginator",true);
        }
        else {
            request.setAttribute("displayPaginator", false);
            request.setAttribute("page",1);
            return 1;
        }
        if (request.getParameter("page") == null) {
            page = 1;
        }
        else {
            page = Integer.parseInt(request.getParameter("page"));
        }
        request.setAttribute("maxPage",maxPage);
        request.setAttribute("page",page);
        return page;
    }
    public int getPage(HttpServletRequest request,int articlesCount) throws Exception {
        int page;
        long maxPageLong = Math.round(Math.ceil((double) articlesCount/getMaxArticlesPerPage()));
        if (maxPageLong > Integer.MAX_VALUE) throw new Exception("Integer not enough in page method:getPage");
        int maxPage =  (int)maxPageLong;
        if (maxPage > 1){
            request.setAttribute("displayPaginator",true);
        }
        else {
            request.setAttribute("displayPaginator", false);
            return 1;
        }
        if (request.getParameter("page") == null){
            page = 1;
        }
        else {
            page = Integer.parseInt(request.getParameter("page"));
        }
        request.setAttribute("maxPage",maxPage);
        request.setAttribute("page",page);
        return page;
    }

    public int getMaxArticlesPerPage(){
        return PropertiesHolder.getInstance().getMaxArticlesPerPage();
    }

    public String getUrl(HttpServletRequest request){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(request.getScheme());
        stringBuilder.append("://");
        stringBuilder.append(request.getServerName());
        stringBuilder.append(":");
        stringBuilder.append(request.getServerPort());
        stringBuilder.append("/");
        stringBuilder.append(request.getContextPath()+"/");
        return stringBuilder.toString();
    }
    public String getImageUrl(HttpServletRequest request){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(request.getScheme());
        stringBuilder.append("://");
        stringBuilder.append(request.getServerName());
        stringBuilder.append(":");
        stringBuilder.append(request.getServerPort());
        stringBuilder.append("/");
        stringBuilder.append(request.getContextPath()+"/");
        stringBuilder.append("images/");
        return stringBuilder.toString();
    }
    public String createAndSetImage(HttpServletRequest request,String fileInputName,String urlInputName)
            throws MyDatabaseException, IOException, ServletException {
        Part filePart = request.getPart("image");
        if (filePart != null) {
            String fileType = filePart.getContentType().substring(0, 5);
            if (fileType.contains("image")) {
                String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
                String extension = fileName.substring(fileName.lastIndexOf('.'));
                InputStream inputStream = filePart.getInputStream();
                String imageName = UUID.randomUUID().toString() + extension;
                File file = new File(request.getServletContext().getRealPath("images/") + imageName);
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                byte[] buffer = new byte[inputStream.available()];
                inputStream.read(buffer);
                fileOutputStream.write(buffer);
                inputStream.close();
                fileOutputStream.close();
                return imageName;
            }

        }
        String imageUrl = request.getParameter("image_url");
        if (( imageUrl != null)&&(imageUrl.matches("^[a-z]{4,5}:\\/\\/[\\w,.,-,\\/,-]+((\\.jpg)|(\\.png))$"))){
            return imageUrl;
        }
        return null;
    }






}
