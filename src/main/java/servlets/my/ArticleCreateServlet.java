package servlets.my;

import model.User;
import service.ArticleService;
import servlets.ArticleServlet;
import utils.AuthorizationToken;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;

@MultipartConfig
public class ArticleCreateServlet extends ArticleServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       /* try {
            AuthorizationToken authorizationToken = handleAuthorization(request);
            if(authorizationToken == null){
                response.sendRedirect("/");
                return;
            }
            ArticleService articleService = new ArticleService();
            String title = request.getParameter("title");
            String description = request.getParameter("description");
            User author = new User(authorizationToken.getUserId(),authorizationToken.getUsername());
            String imageName = "404.jpg";
            //Check image
            String imageNameTemp = createAndSetImage(request,"image","image_url");
            if (imageNameTemp != null){
                imageName = imageNameTemp;
            }
            articleService.create(title,description,author,imageName);
            response.sendRedirect(request.getContextPath()+"/my");
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        catch (Exception ewww){
            ewww.printStackTrace();
        }
    */
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*try {
            if(handleAuthorization(request) == null) {
                response.sendRedirect(request.getContextPath()+"/");
                return;
            }

            request.getRequestDispatcher("/jsp/create_article.jsp").forward(request,response);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        */
    }

}
