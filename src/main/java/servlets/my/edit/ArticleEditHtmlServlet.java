package servlets.my.edit;

import dao.ArticleDao;
import service.ArticleService;
import servlets.ArticleServlet;
import utils.AuthorizationToken;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;

public class ArticleEditHtmlServlet extends ArticleServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            handleAuthorization(request,response);
            editFilter(request,response);
            BufferedReader bufferedReader = request.getReader();
            StringBuilder stringBuilder = new StringBuilder();
            while (bufferedReader.ready()){
                System.out.println("while");
                stringBuilder.append(bufferedReader.readLine());
            }
            String articleHtml = stringBuilder.toString();
            ArticleService articleService = new ArticleService();
            articleService.updateContent(Long.parseLong(request.getParameter("article")),articleHtml);
            response.getWriter().write("done");
        }catch (Exception e){
            handleExeption(request,response,e);

        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            handleAuthorization(request,response);
            editFilter(request,response);
            long articleId = Long.parseLong(request.getParameter("article"));
            ArticleDao articleDao = new ArticleDao();
            String articleHtml = articleDao.getHtmlById(articleId);
            request.setAttribute("articleId",articleId);
            request.setAttribute("articleHtml",articleHtml);
            request.getRequestDispatcher("/jsp/edit_article_html.jsp").forward(request,response);
        }
        catch (SQLException e) {
            handleSQLExeption(request,response,e);
        }
        catch (Exception e){
            handleExeption(request,response,e);
        }

    }
}
