package servlets.my.edit;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import model.User;
import service.ArticleService;
import servlets.ArticleServlet;
import utils.AuthorizationToken;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.List;

public class ArticleCoauthorsAddServlet extends ArticleServlet{
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            //adds user from json
            handleAuthorization(request,response);
            authorFilter(request,response);
            long articleId = Long.parseLong(request.getParameter("article"));
            ArticleService articleService = new ArticleService();
            String json = request.getReader().readLine();
            JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
            long coauthorId = jsonObject.get("id").getAsLong();
            String coauthorUsername = jsonObject.get("username").getAsString();
            articleService.addCoauthor(articleId,getUserId(request),coauthorId);
            String li = String.format("<li onclick=\"deleteCoauthor('%s',this)\">%s (delete icon)</li>",coauthorId,coauthorUsername);
            response.getWriter().write(li);
        }  catch (SQLException e) {
            handleSQLExeption(request,response,e);
        }catch (Exception e2){
            handleExeption(request,response,e2);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            handleAuthorization(request,response);
            authorFilter(request,response);
            long articleId = Long.parseLong(request.getParameter("article"));
            ArticleService articleService = new ArticleService();
            String userNamePart = request.getParameter("username_part");
            List<User> coauthors = articleService.getPossibleCoauthor(articleId,userNamePart);
            StringBuilder stringBuilder = new StringBuilder();
            System.out.println(String.format("my:%s for:%s found:%s users",articleId,userNamePart,coauthors.size()));

            String img = "<img src=\"\" alt\"delete image\" >";
            for (User user:coauthors){
                stringBuilder.append(String.format("<li onclick=\"addCoauthor('%s','%s')\">%s %s</li>",
                        user.getId(),user.getUsername(),user.getUsername(),img));
            }
            response.getWriter().write(stringBuilder.toString());

        } catch (NumberFormatException e){
            handleExeption(request,response,e);
        } catch (SQLException e) {
           handleSQLExeption(request,response,e);
        }
    }
}
