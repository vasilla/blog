package servlets.my.edit;

import model.Article;
import model.User;
import dao.ArticleDao;
import service.ArticleService;
import servlets.ArticleServlet;
import utils.AuthorizationToken;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;

//TODO catch block

//TODO handle Exception
//TODO id - long or uuid
@MultipartConfig
public class ArticleEditServlet extends ArticleServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            handleAuthorization(request,response);
            editFilter(request,response);
            ArticleService articleService = new ArticleService();
            long articleId = Long.parseLong(request.getParameter("article"));
            long userId = getUserId(request);
            String username = (String) request.getAttribute("username");
            ArticleDao articleDao = new ArticleDao();

            String title = request.getParameter("title");
            String description = request.getParameter("description");
            User author = new User(userId,username);
            String imageName = "404.jpg";
            //Check image
            String imageNameTemp = createAndSetImage(request,"image","image_url");
            if (imageNameTemp != null){
                imageName = imageNameTemp;
            }
            articleService.update(userId,articleId,title,description,imageName);
            response.sendRedirect(request.getContextPath()+"/my");
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        catch (Exception ewww){
            ewww.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            handleAuthorization(request,response);
            editFilter(request,response);
            long articleId = Long.parseLong(request.getParameter("article"));
            long userId = (long)request.getAttribute("userId");
            ArticleDao articleDao = new ArticleDao();
            Article article = articleDao.getById(articleId);
            if (articleDao.canDelete(userId,articleId)){
                request.setAttribute("delete",true);
            }
            if (!article.isImageNameFromUrl()){
                request.setAttribute("imagesPath",getImageUrl(request));
            }


            request.setAttribute("article",article);
            request.getRequestDispatcher("/jsp/edit_article.jsp").forward(request,response);
        } catch (NumberFormatException e){
            response.getWriter().write("my parameter broken");
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
