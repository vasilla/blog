package service;

import dao.BlockedDao;

import java.sql.SQLException;

public class BlockedService {
    public String getReason(String blockedId)throws SQLException {
        BlockedDao blockedDao = new BlockedDao();
        return blockedDao.getReason(blockedId);
    }
}
