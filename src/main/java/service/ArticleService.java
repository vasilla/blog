package service;

import dao.BlockedDao;
import model.Article;
import model.Role;
import model.User;
import dao.ArticleDao;
import dao.MyDatabaseException;
import utils.PropertiesHolder;

import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class ArticleService {
    public void create(String title,String description,User user,String imageName) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        Article article = new Article();
        article.setAuthor(user);
        article.setTitle(title);
        article.setDescription(description);
        article.setPublished(false);
        article.setLastUpdated(new Date(Calendar.getInstance().getTimeInMillis()));
        article.setPublishedDate(null);
        article.setImageName(imageName);
        //TODO javax here
        articleDao.create(article);
    }
    public String update(long userId,long articleId,String title,String description,String imageName) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        if (!articleDao.canEdit(userId,articleId)){
            throw new MyDatabaseException("User have no rights to edit this my");
        }

        Article article = articleDao.getById(articleId);
        article.setTitle(title);
        article.setDescription(description);
        article.setImageName(imageName);

        //TODO javax here
        articleDao.update(article);
        return "Successfully done";
    }
    public void publish(long userId,long articleId) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        if (!articleDao.canEdit(userId,articleId)){
            throw new MyDatabaseException("Do not have right to edit");
        }
        articleDao.publish(articleId);
    }
    public void delete(long articleId) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        articleDao.deleteAllCoauthor(articleId);
        articleDao.delete(articleId);

    }

    public List<Article> getPublished(int page) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getPublished(page,PropertiesHolder.getInstance().getMaxArticlesPerPage());
    }

    public int getCountOfPublished() throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getCountOfPublished();
    }

    public List<Article> getMy(long userId,int page) throws SQLException{
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getMy(userId,page,PropertiesHolder.getInstance().getMaxArticlesPerPage());
    }

    public int getCountOfMy(long userId) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getCountOfMy(userId);
    }
    public void updateContent(long articleId,String contentHtml) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        articleDao.updateContent(articleId,contentHtml);
    }
    public void addCoauthor(long articleId,long userId,long coauthorId) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        Article article = articleDao.getById(articleId);
        if (article.getAuthor().getId() == userId) {
            articleDao.addCoauthor(articleId, coauthorId);
        }
        else throw new MyDatabaseException("You have no rights for this");
    }
    public void deleteCoauthor(long articleId,long userId) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        Article article = articleDao.getById(articleId);
        if (article.getAuthor().getId() == userId) {
            articleDao.deleteCoauthor(articleId, userId);
        }
        else throw new MyDatabaseException("You have no rights for this");
    }
    public List<User> getCoauthor(long articleId) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getCoauthor(articleId);
    }
    public List<User> getPossibleCoauthor(long articleId,String usernamePart) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getPossibleCoauthor(articleId,usernamePart,PropertiesHolder.getInstance().getMaxSearchResults());
    }

    public List<Article> searchByTitleMinimum(String titlePart,int limit) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.searchByTitleMinimum(titlePart,PropertiesHolder.getInstance().getMaxSearchResults());
    }
    public int getCountOfSearchByTitle(String titlePart) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getCountOfSearchByTitle(titlePart);
    }
    public List<Article> searchByTitle(String titlePart,long page) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.searchByTitle(titlePart,page,PropertiesHolder.getInstance().getMaxArticlesPerPage());
    }
    public void setBlocked(long articleId,long user,String reason) throws SQLException {
        String blockedId = UUID.randomUUID().toString();
        ArticleDao articleDao = new ArticleDao();
        articleDao.setBlocked(articleId,blockedId);
        BlockedDao blockedDao = new BlockedDao();
        blockedDao.addBlocked(blockedId,reason,user);
    }
    public void setUnBlocked(long articleId) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        Article article = articleDao.getById(articleId);
        articleDao.setUnblocked(articleId);
        BlockedDao blockedDao = new BlockedDao();
        blockedDao.deleteBlocked(article.getBlockedId());

    }
    public List<Article> searchByTitleForAdmin(String titlePart,int page,int articlesPerPage) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.searchByTitleForAdmin(titlePart,page,articlesPerPage);
    }
    public int getCountOfSearchByTitleForAdmin(String titlePart) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getCountOfSearchByTitleForAdmin(titlePart);
    }
    public List<Article> getAllForAdmin(int page,int articlesPerPage) throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getAllForAdmin(page,articlesPerPage);
    }
    public int getCountOfPublishedForAdmin() throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getCountOfPublishedForAdmin();
    }

    public Article getById(long id)throws SQLException {
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getById(id);
    }
    public String getHtmlById(long articleId) throws SQLException{
        ArticleDao articleDao = new ArticleDao();
        return articleDao.getHtmlById(articleId);
    }









}
