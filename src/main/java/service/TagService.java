package service;

import model.Tag;
import dao.TagDao;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class TagService {
    public void updateTags(long userId,long articleId,List<Long> tagsId) throws SQLException {
        //TODO check if can edit
        TagDao tagDao = new TagDao();
        System.out.println("///////TAGS"+ Arrays.toString(tagsId.toArray()));
        tagDao.deleteArticleTag(articleId);
        for(Long tagId :tagsId){
            tagDao.addArticleTag(articleId,tagId);
        }

    }
    public List<Tag> getForArticle(long articleId) throws SQLException {
        TagDao tagDao = new TagDao();
        return tagDao.getForArticle(articleId);
    }

    public void saveChanges(Tag tag) throws SQLException {
        TagDao tagDao = new TagDao();
        if (tag.getId() == -1){
            tagDao.add(tag.getName(),tag.getAuthor().getId());
        }
        else {
            tagDao.update(tag.getId(), tag.getName(), tag.getAuthor().getId());
        }
    }

    public void delete(long id) throws SQLException {
        TagDao tagDao = new TagDao();
        tagDao.delete(id);
    }
    public List<Tag> getForAdmin(int page,int maxPerPage) throws SQLException{
        TagDao tagDao = new TagDao();
        return tagDao.getForAdmin(page, maxPerPage);
    }

    public int getCount()throws SQLException{
        TagDao tagDao = new TagDao();
        return tagDao.getCount();
    }

}
