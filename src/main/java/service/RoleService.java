package service;


import dao.RoleDao;
import model.Role;


import java.sql.SQLException;
import java.util.List;

public class RoleService {

    public void saveChanges(Role role) throws SQLException {
        RoleDao roleDao = new RoleDao();
        if (role.getId() == -1){
            roleDao.add(role);
        }
        else {
            roleDao.update(role);
        }
    }
    public void delete(long id) throws SQLException {
        RoleDao roleDao = new RoleDao();
        roleDao.delete(id);
    }
    public List<Role> getForAdmin(int page, int maxPerPage) throws SQLException{
        RoleDao roleDao = new RoleDao();
        return roleDao.getForAdmin(page, maxPerPage);
    }

    public int getCount()throws SQLException{
        RoleDao roleDao = new RoleDao();
        return roleDao.getCount();
    }
    public List<Role> getRoleNames() throws SQLException {
        RoleDao roleDao = new RoleDao();
        return roleDao.getRoleNames();
    }
}
