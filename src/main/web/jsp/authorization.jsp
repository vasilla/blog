<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 15.07.2018
  Time: 15:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
</head>
<body>
    <jsp:include page="template/header.jsp"/>
    <div class="container">
        <form action="${pageContext.request.contextPath}/authorization" method="post" class="authorization_form">
            <div class="form-group">
                <label for="login">Login</label>
                <input class="form-control" type="text" name="login" id="login" placeholder="Enter login">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" id="password" placeholder="Enter password">
            </div>
            <input type="submit" value="log in" class="btn btn-primary">
        </form>
    </div>
    <jsp:include page="template/footer.jsp"/>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>
</html>
