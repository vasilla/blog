<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
</head>
<body>
    <jsp:include page="template/header.jsp"/>
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid ">
                <ul class="nav navbar-nav">
                    <li><a href="${pageContext.request.contextPath}/article/edit?article=${articleId}">article information</a></li>
                    <li><a href="${pageContext.request.contextPath}/article/edit/html?article=${articleId}">page html</a></li>
                    <li><a href="${pageContext.request.contextPath}/article/edit/tags?article=${articleId}">tags</a></li>
                    <li><a href="#">coauthors</a></li>
                </ul>
            </div>
        </nav>
        <div id="edit_coauthors">
            <div>
                <ul id="edit_coauthors_list">
                    <c:forEach var="user" items="${coauthors}">
                        <li onclick="deleteCoauthor('${user.id}',this)">${user.username}</li>
                    </c:forEach>
                </ul>
            </div>
            <div>
                <label for="coauthor_name">Write coauthor name</label>
                <input type="text" id="coauthor_name" onkeydown="getPossibleCoauthor(this.value)" onkeyup="getPossibleCoauthor(this.value)">
            </div>
            <div>
                <ul id="add_coauthors_list">

                </ul>
            </div>




        </div>
    </div>
    <jsp:include page="template/footer.jsp"/>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/edit_article_coauthors.js"></script>
    <script>
        setArticleId(${articleId},'${pageContext.request.contextPath}');
        getPossibleCoauthor("");
    </script>
</body>
</html>
