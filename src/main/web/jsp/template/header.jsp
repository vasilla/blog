<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar navbar-inverse ">
    <div class="container-fluid ">
        <div class="navbar-header">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/">Blog</a>
        </div>
        <ul class="nav navbar-nav">
            <%--<li class="active"><a href="#">Home</a></li>--%>
            <li><a href="${pageContext.request.contextPath}/">Main</a></li>
            <li><a href="${pageContext.request.contextPath}/my">My</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin panel</a>
                <ul class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/admin/articles">Articles</a></li>
                    <li><a href="${pageContext.request.contextPath}/admin/users">Users</a></li>
                    <li><a href="${pageContext.request.contextPath}/admin/tags">Tags</a></li>
                    <li><a href="${pageContext.request.contextPath}/admin/roles">Roles</a></li>
                </ul>
            </li>
        </ul>
        <c:if test="${not logged}">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Sign Up</a></li>
                <li><a href="${pageContext.request.contextPath}/authorization">Login </a></li>
            </ul>
        </c:if>
        <c:if test="${logged}">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">${username}</a>
                    <ul class="dropdown-menu">
                        <li><a href="${pageContext.request.contextPath}/logout">log out</a></li>
                    </ul>
                </li>
            </ul>
        </c:if>

    </div>
</nav>
</div>
