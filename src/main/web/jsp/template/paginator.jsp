<%--
  Created by IntelliJ IDEA.
  User: vchyztc
  Date: 05.07.2018
  Time: 15:57
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${displayPaginator}">
    <%--<nav >--%>
        <%--<ul class="pagination">--%>
            <%--<li class="page-item"><a class="page-link" href="#">Previous</a></li>--%>
            <%--<li class="page-item"><a class="page-link" href="#">1</a></li>--%>
            <%--<li class="page-item"><a class="page-link" href="#">2</a></li>--%>
            <%--<li class="page-item"><a class="page-link" href="#">3</a></li>--%>
            <%--<li class="page-item"><a class="page-link" href="#">Next</a></li>--%>
        <%--</ul>--%>
    <%--</nav>--%>
    <nav>
        <ul class="pagination">
            <%--<li class="page-item">page ${page} of ${maxPage}</li>--%>
            <c:if test="${page>2}">
                <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}${url_prefix}?page=1">first page</a></li>
            </c:if>
            <c:if test="${page>1}">
                <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}${url_prefix}?page=${page-1}">previous page </a></li>
</c:if>
            <c:if test="${page-2>=1}">
                <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}${url_prefix}?page=${page-2}">${page-2}</a></li>
            </c:if>
            <c:if test="${page-1>=1}">
                <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}${url_prefix}?page=${page-1}">${page-1}</a></li>
            </c:if>
            <li class="page-item active"><a class="page-link" href="#">${page}<span class="sr-only"></span></a></li>
            <c:if test="${page+1<=maxPage}">
                <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}${url_prefix}?page=${page+1}">${page+1}</a></li>
            </c:if>
            <c:if test="${page+2<=maxPage}">
                <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}${url_prefix}?page=${page+2}">${page+2}</a></li>
            </c:if>
            <c:if test="${page+1<=maxPage}">
                <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}${url_prefix}?page=${page+1}">next page</a></li>
            </c:if>
            <c:if test="${page+1<maxPage}">
                <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}${url_prefix}?page=${maxPage}">last page</a></li>
            </c:if>
        </ul>
    </nav>
</c:if>