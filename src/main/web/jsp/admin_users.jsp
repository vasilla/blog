<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
</head>
<body>
    <jsp:include page="template/header.jsp"/>
    <div class="container">
        <h3>admin users jsp</h3>
        <table class="table">
            <thead>
            <tr>
                <td>Username</td>
                <td>Role</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody id="table_body">
            <c:forEach var="user" items="${users}">
                <tr>
                    <td>${user.getUsername()}</td>
                    <c:if test="${not can_update_role}">
                        <td>${user.getRole().getName()}</td>
                    </c:if>
                    <c:if test="${can_update_role}">
                        <c:if test="${user.getId() eq userId}">
                            <td>${user.getRole().getName()}</td>
                        </c:if>
                        <c:if test="${not(user.getId() eq userId)}">
                            <td><input type="button" onclick="updateUserRight('${user.getId()}','${user.getRole().getId()}','${user.getUsername()}')" value="${user.getRole().getName()}" class="btn btn-secondary"> </td>
                        </c:if>
                    </c:if>


                    <c:if test="${user.isBlocked()}">

                            <td><input type="button" onclick="unBlockUser('${user.getId()}','${user.getUsername()}')" value="unblock" class="btn btn-secondary"> </td>




                    </c:if>
                    <c:if test="${not user.isBlocked()}">
                        <c:if test="${not(user.getId() eq userId)}">
                        <td><button  class="btn btn-primary"
                                     onclick="blockUser('${user.getId()}','${user.getUsername()}')">block</button></td>
                        </c:if>

                        <c:if test="${(user.getId() eq userId)}">
                            <td></td>
                        </c:if>

                    </c:if>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <jsp:include page="template/paginator.jsp"/>
    </div>

    <jsp:include page="template/footer.jsp"/>
    <div class="modal fade" id="blockModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal1Label">User block menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="reason_input">Enter reason</label>
                    <textarea  id="reason_input" class="form-control"  rows="4" ></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="blockUserSend()">Block user</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updateRightsModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal2Label">User:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="role_input">Select new role</label>
                    <select  id="role_input" class="form-control" >
                        <c:forEach var="role" items="${roles}">
                            <option value="${role.getId()}">${role.getName()}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updateRoleSend()">Update role</button>
                </div>
            </div>
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/admin_users.js"></script>
    <c:if test="${can_update_role}">
        <script src="${pageContext.request.contextPath}/js/admin_users_roles_update.js"></script>
    </c:if>
    <script>
        setPage("${page}")
        setUrlPrefix2("${pageContext.request.contextPath}/admin/users")
        setUrlPrefix3("${pageContext.request.contextPath}/admin/users/updateRole")
    </script>
</body>
</html>
