<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
</head>
<body>
    <jsp:include page="template/header.jsp"/>

    <div class="container">
        <a href="${pageContext.request.contextPath}/my/create"  class="btn btn-primary btn-lg add_button" >Create new article</a>


        <c:forEach var="article" items="${articles}" varStatus="loop">
        <c:if test="${loop.index%2==0}">
        <div class="row">
            </c:if>
            <div class="col-sm-6">
                <div class="card" >
                    <c:if test="${not article.isImageNameFromUrl()}">
                        <img class="card-img-top img-fluid" src="${imagesPath}${article.getImageName()}"  >
                    </c:if>
                    <c:if test="${article.isImageNameFromUrl()}">
                        <img class="card-img-top img-fluid" src="${article.getImageName()}"  >
                    </c:if>
                    <h5 class="card-title">${article.title}</h5>
                    <p class="card-text">${article.description}</p>
                    <a href="${pageContext.request.contextPath}/view?article=${article.id}" class="btn btn-primary">Details</a>
                    <a href="${pageContext.request.contextPath}/article/edit?article=${article.id}" class="btn btn-primary">Edit</a>
                    <c:if test="${article.getAuthor().getId() eq userId}">
                        <input type="button" value="Delete" class="btn btn-danger" onclick="confirmDelete(${article.id})">
                    </c:if>
                    <%--<div class="btn-group">--%>
                        <%--<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--%>
                            <%--edit--%>
                        <%--</button>--%>
                        <%--<div class="dropdown-menu">--%>
                            <%--<a class="dropdown-item" href="#">Informat</a>--%>
                            <%--<a class="dropdown-item" href="#">Another action</a>--%>
                            <%--<a class="dropdown-item" href="#">tags</a>--%>
                            <%--<a class="dropdown-item" href="#">coauthors</a>--%>
                        <%--</div>--%>
                    <%--</div>--%>

                </div>
            </div>
            <c:if test="${loop.index%2==1}">
        </div>
        </c:if>
        <c:if test="${loop.index%2==0}">
        <c:if test="${loop.isLast()}">
    </div>
    </c:if>
    </c:if>
    </c:forEach>



    <jsp:include page="template/paginator.jsp"/>
    </div>
    </div>

    <jsp:include page="template/footer.jsp"/>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/confirm.js"></script>
    <script>
        setUrlPrefix("${urlPrefix}");
    </script>

</body>
</html>
