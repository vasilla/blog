<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
</head>
<body>
    <jsp:include page="template/header.jsp"/>
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid ">
                <ul class="nav navbar-nav">
                    <li><a href="${pageContext.request.contextPath}/article/edit?article=${articleId}">article information</a></li>
                    <li><a href="${pageContext.request.contextPath}/article/edit/html?article=${articleId}">page html</a></li>
                    <li class="active"><a href="#">tags</a></li>
                    <li><a href="${pageContext.request.contextPath}/article/coauthors?article=${articleId}">coauthors</a></li>
                </ul>
            </div>
        </nav>

        <div id="edit_tags" class="form-group">
            <label for="tags_select">Select tags</label>
            <select name="" id="tags_select" class="form-control" multiple>
                <c:forEach var="tag" items="${tags}">
                    <option
                            <c:if test="${tag.selected}">
                                class="tag_option selected" selected="true"
                            </c:if>
                            <c:if test="${not tag.selected}">
                                class="tag_option"
                            </c:if>

                            value="${tag.id}"

                    >${tag.name}</option>

                </c:forEach>
            </select>
            <input type="button" value="update tags" onclick="updateTags()" class="btn btn-primary">
        </div>
    </div>
    <jsp:include page="template/footer.jsp"/>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/edit_tags.js"></script>

    <script>
        setUrl("${pageContext.request.contextPath}/article/edit/tags?article=${articleId}");
        setArticleId(${articleId},'${pageContext.request.contextPath}');
        fixTagOptions();
    </script>
</body>
</html>
