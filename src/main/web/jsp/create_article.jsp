<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
</head>
<body>
    <jsp:include page="template/header.jsp"/>
    <div class="container">


        <form action="/article/create" method="post" enctype="multipart/form-data">
            <div class="form-group ">
                <label for="title_input">Title</label>
                <input type="text" name="title" id="title_input" class="form-control"  maxlength="60">
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="image_url"> set image by url</label>
                    <input type="text" name="image_url" id="image_url" class="form-control"  onchange="document.getElementById('image_preview').src =
                     this.value">
                </div>
                <div class="form-group col-md-6">
                    <label for="image_file">Or set image by file</label>
                    <input type="file"  name="image" id="image_file" class="form-control"
                           onchange="document.getElementById('image_preview').src =window.URL.createObjectURL(this.files[0])"
                >
                </div>
            </div>
            <div class="img-div">
                <img src=""  id="image_preview" name="image" class="img-fluid">
            </div>
            <label for="description_input"> enter description</label>
            <textarea name="description" id="description_input" class="form-control"  rows="2" maxlength="150"></textarea>
            <input type="submit" value="create article" class="btn btn-primary">
        </form>
    </div>

    <jsp:include page="template/footer.jsp"/>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</body>
</html>
