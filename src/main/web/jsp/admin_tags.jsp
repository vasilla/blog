<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
</head>
<body>
    <jsp:include page="template/header.jsp"/>

    <div class="container">
        <input type="button" onclick="set('-1','create new')" value="add" class="btn btn-primary btn-lg add_button">
        <table class="table">
            <thead>
            <tr>
                <td>Tag name</td>
                <td>Author</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody id="table_body">
            <c:forEach var="tag" items="${tags}">
                <tr>
                    <td>${tag.getName()}</td>
                    <td>${tag.getAuthor().getUsername()}</td>
                    <td><input type="button" onclick="set('${tag.getId()}','${tag.getName()}')" value="edit" class="btn btn-secondary"></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>


        <jsp:include page="template/paginator.jsp"/>
    </div>
    <div class="modal fade" id="blockModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="tag_input">Enter tag name</label>
                    <input type="text" id="tag_input" class="form-control">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save()">Save</button>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="template/footer.jsp"/>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/admin_tags.js"></script>
    <script>
        setPage("${page}")
        setUrlPrefix2("${pageContext.request.contextPath}/admin/tags")
    </script>
</body>
</html>
