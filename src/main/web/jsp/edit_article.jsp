<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
</head>
<body>
    <jsp:include page="template/header.jsp"/>
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid ">
                <ul class="nav navbar-nav">
                    <li class="active"><a  href="#">article information</a></li>
                    <li><a href="${pageContext.request.contextPath}/article/edit/html?article=${article.id}">page html</a></li>
                    <li><a href="${pageContext.request.contextPath}/article/edit/tags?article=${article.id}">tags</a></li>
                    <li><a href="${pageContext.request.contextPath}/article/coauthors?article=${article.id}">coauthors</a></li>
                </ul>
            </div>
        </nav>
        <form action="${pageContext.request.contextPath}/article/edit?article=${article.id}" method="post" enctype="multipart/form-data">
            <div class="form-group ">
                <label for="title_input">Title</label>
                <input type="text" name="title" id="title_input" value="${article.title}" class="form-control"  maxlength="60">
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="image_url"> set image by url</label>
                    <input type="text" name="image_url" id="image_url" class="form-control"  onchange="document.getElementById('image_preview').src =
                         this.value"
                    <c:if test="${article.isImageNameFromUrl()}">
                        value="${article.getImageName()}"
                    </c:if>

                    >
                </div>
                <div class="form-group col-md-6">
                    <label for="image_file">Or set image by file</label>
                    <input type="file"  name="image" id="image_file" class="form-control"
                           onchange="document.getElementById('image_preview').src =window.URL.createObjectURL(this.files[0])"
                    >
                </div>
            </div>
            <div class="img-div">

                <c:if test="${not article.isImageNameFromUrl()}">
                    <img src="${imagesPath}${article.getImageName()}"  id="image_preview" name="image" class="img-fluid">
                </c:if>
                <c:if test="${article.isImageNameFromUrl()}">
                    <img src="${article.getImageName()}"  id="image_preview" name="image" class="img-fluid">
                </c:if>
            </div>
            <div class="form-group ">
                <label for="description_input"> enter description</label>
                <textarea name="description" id="description_input" class="form-control"  rows="2" maxlength="150">${article.description}</textarea>
            </div>
            <input type="submit" value="update article" class="btn btn-primary">
        </form>
        <c:if test="${delete}">
            <c:if test="${not article.isPublished()}">
                <input type="button" value="publish" class="btn btn-primary" onclick="confirmPublish()">
            </c:if>
        </c:if>
        <c:if test="${delete}">
            <input type="button" value="delete article" class="btn btn-danger" onclick="confirmDelete()">
        </c:if>
    </div>


    <jsp:include page="template/footer.jsp"/>


    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/confirm.js"></script>
    <script>
        setArticleId(${article.id},'${pageContext.request.contextPath}');
    </script>
</body>
</html>
