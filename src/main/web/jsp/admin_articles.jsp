<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
</head>
<body>
    <jsp:include page="template/header.jsp"/>
    <div class="container">
        <%--<label for="article_search">Write article </label>--%>
        <%--<input type="text" id="article_search" onkeyup="articleSearch()" onkeydown="articleSearch()">--%>


        <table class="table">
            <thead>
                <tr>
                    <td>Title</td>
                    <td>Published date</td>
                    <td>Author</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody id="table_body">
            <c:forEach var="article" items="${articles}">
                <tr>
                    <td>${article.getTitle()}</td>
                    <td>${article.getPublishedDate().toString()}</td>
                    <td>${article.getAuthor().getUsername()}</td>
                    <c:if test="${article.isBlocked()}">
                        <td><input type="button" onclick="unBlockArticle('${article.id}','${article.getTitle()}')" value="unblock" class="btn btn-secondary"> </td>


                    </c:if>
                    <c:if test="${not article.isBlocked()}">
                        <td><button  class="btn btn-primary"
                                     onclick="blockArticle('${article.id}','${article.getTitle()}')">block</button></td>

                    </c:if>
                </tr>
            </c:forEach>


            </tbody>
        </table>

        <jsp:include page="template/paginator.jsp"/>
    </div>


    <div class="modal fade" id="blockModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="reason_input">Enter reason</label>
                    <textarea  id="reason_input" class="form-control"  rows="4" ></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="blockArticleSend()">Block article</button>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="template/footer.jsp"/>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/admin_articles.js"></script>
    <script>
        setPage("${page}")
        setUrlPrefix2("${pageContext.request.contextPath}/admin/articles")
    </script>
</body>
</html>
