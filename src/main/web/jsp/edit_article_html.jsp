<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/trumbowyg/dist/ui/trumbowyg.min.css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
</head>
<body>
    <jsp:include page="template/header.jsp"/>
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid ">
                <ul class="nav navbar-nav">
                    <li><a href="${pageContext.request.contextPath}/article/edit?article=${articleId}">article information</a></li>
                    <li class="active"><a href="#">page html</a></li>
                    <li><a href="${pageContext.request.contextPath}/article/edit/tags?article=${articleId}">tags</a></li>
                    <li><a href="${pageContext.request.contextPath}/article/coauthors?article=${articleId}">coauthors</a></li>
                </ul>
            </div>
        </nav>
        <div id="edit_content">
            <label for="trumbowyg-editor">Write page and click <strong>Save</strong> </label>
            <div id="trumbowyg-editor"></div>
        </div>
    </div>
    <jsp:include page="template/footer.jsp"/>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/trumbowyg/dist/trumbowyg.min.js"></script>
    <script type="application/json" src="${pageContext.request.contextPath}/trumbowyg/dist/plugins/upload/trumbowyg.cleanpaste.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/edit_article_html.js"></script>
    <script>
        setUrl('${pageContext.request.contextPath}/article/edit/html?article=${articleId}');
        initTrumbowyg("${articleHtml}");

    </script>
</body>
</html>
