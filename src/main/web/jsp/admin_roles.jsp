<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">
</head>
<body>
    <jsp:include page="template/header.jsp"/>

    <div class="container">
        <input type="button" onclick="set('-1','create new')" value="add" class="btn btn-primary btn-lg add_button">
        <table class="table">
            <thead>
            <tr>
                <td>Role name</td>
                <td>block users</td>
                <td>block article</td>
                <td>edit roles</td>
                <td>edit personnel</td>
                <td>edit tags</td>
                <td>Action</td>
            </tr>
            </thead>
            <tbody id="table_body">
            <c:forEach var="role" items="${roles}">
                <tr>
                    <td>${role.getName()}</td>
                    <td><input type="checkbox"
                        <c:if test="${role.isCanBlockUsers()}">
                                   checked="true"
                        </c:if>
                    ></td>
                    <td><input type="checkbox"
                    <c:if test="${role.isCanBlockArticle()}">
                               checked="true"
                    </c:if>
                    ></td>
                    <td><input type="checkbox"
                    <c:if test="${role.isCanEditRoles()}">
                               checked="true"
                    </c:if>
                    ></td>
                    <td><input type="checkbox"
                    <c:if test="${role.isCanEditPersonnel()}">
                               checked="true"
                    </c:if>
                    ></td>
                    <td><input type="checkbox"
                    <c:if test="${role.isCanEditTag()}">
                               checked="true"
                    </c:if>
                    ></td>
                    <td><input type="button" onclick="set('${role.getId()}','${role.getName()}',
                            '${role.isCanBlockUsers()}','${role.isCanBlockArticle()}','${role.isCanEditRoles()}',
                            '${role.isCanEditPersonnel()}','${role.isCanEditTag()}')" value="edit"
                               class="btn btn-secondary"></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>


        <jsp:include page="template/paginator.jsp"/>
    </div>
    <div class="modal fade" id="blockModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="role_input">Enter role name</label>

                    <input type="text" id="role_input" class="form-control" >
                    <label for="mb1">block users</label>
                    <input type="checkbox" id="mb1" class="form-control" value="block users">
                    <label for="mb2">block article</label>
                    <input type="checkbox" id="mb2" class="form-control" value="block article">
                    <label for="mb3">edit roles</label>
                    <input type="checkbox" id="mb3" class="form-control" value="edit roles">
                    <label for="mb4">edit personnel</label>
                    <input type="checkbox" id="mb4" class="form-control" value="edit personnel">
                    <label for="mb5">edit tags</label>
                    <input type="checkbox" id="mb5" class="form-control" value="edit tags">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save()">Save</button>
                </div>
            </div>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/admin/roles" method="post" class="invisible">
        <input type="text" name="page" value="${page}">
        <input type="text" name="name" id="f_name">
        <input type="text" name="id" id="f1_id">
        <input type="checkbox" name="block_users" id="f_b1">
        <input type="checkbox" name="block_article" id="f_b2">
        <input type="checkbox" name="edit_roles" id="f_b3">
        <input type="checkbox" name="edit_personnel" id="f_b4">
        <input type="checkbox" name="edit_tags" id="f_b5">
        <input type="submit" id="f1_submit">
    </form>
    <form action="${pageContext.request.contextPath}/admin/roles" method="post" class="invisible">
        <input type="text" name="page" value="${page}">
        <input type="text" name="id" id="f2_id">
        <input type="text" name="delete" value="true">
        <input type="submit" id="f2_submit">
    </form>

    <jsp:include page="template/footer.jsp"/>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/admin_roles.js"></script>
</body>
</html>
