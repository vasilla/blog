<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>

    <title>Document</title>
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--%>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--%>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
    <link  rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css">


</head>
<body>
    <jsp:include page="template/header.jsp"/>
    <div class="container">
                <c:forEach var="article" items="${articles}" varStatus="loop">
                    <c:if test="${loop.index%2==0}">
                            <div class="row">
                    </c:if>
                            <div class="col-sm-6">
                                <div class="card" >
                                    <c:if test="${not article.isImageNameFromUrl()}">
                                        <img class="card-img-top img-fluid" src="${imagesPath}${article.getImageName()}"  >
                                    </c:if>
                                    <c:if test="${article.isImageNameFromUrl()}">
                                        <img class="card-img-top img-fluid" src="${article.getImageName()}"  >
                                    </c:if>
                                    <h5 class="card-title">${article.title}</h5>
                                    <p class="card-text">${article.description}</p>
                                    <a href="${pageContext.request.contextPath}/view?article=${article.id}" class="btn btn-primary">Details</a>
                                </div>
                            </div>
                  <c:if test="${loop.index%2==1}">
                            </div>
                  </c:if>
                    <c:if test="${loop.index%2==0}">
                        <c:if test="${loop.isLast()}">
                            </div>
                        </c:if>
                    </c:if>
                </c:forEach>

        <jsp:include page="template/paginator.jsp"/>
        </div>
    </div>

    <jsp:include page="template/footer.jsp"/>
    <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>




</body>
</html>