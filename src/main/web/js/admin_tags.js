
var currentId;
var currentPage;
var urlPrefix;
function setUrlPrefix2(urlPrefix2) {
    urlPrefix = urlPrefix2;
}
function setPage(page) {
    currentPage = page;
}
function save() {
    var input = document.getElementById("tag_input").value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            location.reload();
        }
    };
    xhttp.open("POST", urlPrefix, true);
    var json = JSON.stringify({"id": currentId,"name":input,"page":currentPage});
    xhttp.send(json);
}
function set(id,articleTitle) {
    currentId = id;
    document.getElementById('modalLabel').innerText = "Tag: "+articleTitle;
    $('#blockModal').modal('show');
}

