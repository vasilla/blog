var articleId;
var urlPrefix;
function setUrl(url2) {
    url = url2;
}
function setArticleId(id,urlPrefix) {
    console.log("setArticleId worked");
    articleId = id;

}

function deleteCoauthor(coauthorId,el) {
    if(confirm("Do you want to delete this coauthor?")) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                el.remove();
                getPossibleCoauthor(document.getElementById("coauthor_name").value);
            }
        };
        xhttp.open("POST", "articles/coauthors?articles=" + articleId, true);
        xhttp.send(coauthorId);
    }
}

function addCoauthor(coauthorId,username) {
    if(confirm("Do you want to add this coauthor?")) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var list = document.getElementById("edit_coauthors_list");
                list.innerHTML = list.innerHTML + this.responseText;
                getPossibleCoauthor(document.getElementById("coauthor_name").value);
            }
        };
        xhttp.open("POST", "articles/coauthors/add?articles=" + articleId, true);
        xhttp.send(JSON.stringify({"id":coauthorId,"username":username}));
    }
}
function getPossibleCoauthor(text) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("add_coauthors_list").innerHTML = this.responseText;

        }
    };
    xhttp.open("GET", "articles/coauthors/add?articles=" + articleId+"&username_part="+text, true);
    xhttp.send();


}

