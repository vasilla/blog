
var currentId;
var currentPage;
var urlPrefix;
function setUrlPrefix2(urlPrefix2) {
    urlPrefix = urlPrefix2;
}
function setPage(page) {
    currentPage = page;
}
function blockArticleSend() {
    var reason = document.getElementById("reason_input").value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            location.reload();
        }
    };
    xhttp.open("POST", urlPrefix, true);
    var json = JSON.stringify({"id": currentId,"reason":reason,"page":currentPage});
    console.log(json);
    xhttp.send(json);
}
function blockArticle(id,articleTitle) {
    currentId = id;
    console.log("block:"+id);
    document.getElementById('modalLabel').innerText = "Article: "+articleTitle;
    $('#blockModal').modal('show');
}
function unBlockArticle(id,articleTitle) {
    if (confirm("Are you sure want to unblock "+articleTitle + " ?")) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                location.reload();
            }
        };

        xhttp.open("POST", urlPrefix, true);
        var json = JSON.stringify({"id": id,"page":currentPage});
        console.log(json)
        xhttp.send(json);
    }
}
