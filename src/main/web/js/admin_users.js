var currentId;
var currentPage;
var urlPrefix;
function setUrlPrefix2(urlPrefix2) {
    urlPrefix = urlPrefix2;
}
function setPage(page) {
    currentPage = page;
    console.log("page:"+page)
}
function blockUserSend() {
    var reason = document.getElementById("reason_input").value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            location.reload();
        }
    };
    xhttp.open("POST", urlPrefix, true);
    var json = JSON.stringify({"id": currentId,"reason":reason,"page":currentPage});
    console.log(json);
    xhttp.send(json);
}
function blockUser(id,username) {
    currentId = id;
    document.getElementById("modal1Label").innerText = "User: "+username;
    console.log("block:"+id);
    $('#blockModal').modal('show');
}
function unBlockUser(id,username) {
    if (confirm("Are you sure want to unblock "+username+" ?")) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                location.reload();
            }
        };

        xhttp.open("POST", urlPrefix, true);
        var json = JSON.stringify({"id": id,"page":currentPage});
        console.log(json)
        xhttp.send(json);
    }
}