var articleId;
var urlPrefix;
var url;

function setArticleId(id,urlPrefix) {
    console.log("setArticleId worked");
    articleId = id;

}

function setUrl(url2) {
    url = url2;
}
function updateTags() {
    var optionsAll = document.getElementsByClassName("tag_option");
    var tagsList = [];
    for (var i = 0; i < optionsAll.length ; i++) {
        if (optionsAll[i].classList.contains('selected')){
            tagsList.push(optionsAll[i].value);
        }
    }
    var json = JSON.stringify(tagsList);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            alert(this.responseText);
        }
    };

    xhttp.open("POST",url, true);
    xhttp.send(json);

}

function fixTagOptions() {
    $('.tag_option').mousedown(function(e) {
        e.preventDefault();
        jQuery(this).toggleClass('selected');
        jQuery(this).prop('selected', !jQuery(this).prop('selected'));
        return false;
    });
}

