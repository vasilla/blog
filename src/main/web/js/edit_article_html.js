var articleId;


var url;
function setArticleId(id,urlPrefix) {
    console.log("setArticleId worked");
    articleId = id;

}
function setUrl(url2) {
    url = url2;
}
function saveData() {
    var htmlText = $('#trumbowyg-editor').trumbowyg('html');
    console.log(htmlText);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
        }
    };
    console.log(url)
    xhttp.open("POST", url, true);
    xhttp.send(htmlText);
}

function initTrumbowyg(html) {
    $('#trumbowyg-editor').trumbowyg({
        minimalLinks: true,
        imageWidthModalEdit: true,

        btnsDef: {

            save: {
                fn: saveData,
                title:'Save',
                hasIcon: false
            }
        },
        btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['save'],
            ['fullscreen']
        ],
        plugins: {
            resizimg : {
                minSize: 64,
                step: 16
            }
        }
    });
    $('#trumbowyg-editor').trumbowyg('html', html);

}

